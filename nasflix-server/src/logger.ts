import { createLogger, transports, format } from 'winston';

import { load as loadConfig } from 'node-config-yaml';

const { combine, timestamp, prettyPrint, printf } = format;
const config = loadConfig('./config/config.yml');
const instanceId = process.env.INSTANCE_ID ?? `${config.app.name}-${process.pid}`;

const lineFormat = printf(
  ({timestamp, level, instanceId, label, message}) =>
    `[${timestamp} | ${level.toUpperCase()} | ${instanceId} ${label ? (`| ${label} ` ) : ''}]  ${message}`
);

const childLoggers = {};
const rootLogger = createLogger({
  level: config.app.logging.level,
  format: combine(
    timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSS'}),
    prettyPrint(),
    lineFormat
  ),
  defaultMeta: {
    instanceId
  },
  transports: [ new transports.Console()]
});

export const getLogger = (label?) => {
  if(label) {
    let childLogger = childLoggers[label];
    if(!childLogger) {
      childLogger = rootLogger.child({label});
      childLoggers[label] = childLogger;
    }
    return childLogger;
  } else {
    return rootLogger;
  }
}
