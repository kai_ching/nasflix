export type HttpError = {
  message: string;
}

export type Paginated<T> = {
  docs: T[];
  offset: number;
  limit: number;
  docCount: number;
}

export type DocumentIdentifier = {
  id: string;
}

export type DocumentCollection = {
  id: string[];
}

export type SearchParams = {
  q?: string;
  offset?: number;
  limit?: number;
}
