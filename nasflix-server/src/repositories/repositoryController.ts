import { Types, Schema } from 'mongoose';
import { Repository, model as RepositoryModel } from './Repository';
import { findMany as findManyAssets, remove as removeAssets } from '../assets/assetController';

const ObjectId = Types.ObjectId;

type UpdateParams = {
  location?: string;
  name?: string;
  description: string;
}

export const create = async (repository: Repository): Promise<Repository> => {
  return RepositoryModel.create<Repository>(repository);
}

export const findById = async (id: string | Schema.Types.ObjectId): Promise<Repository> => {
  if (id instanceof ObjectId || (typeof id === 'string' && ObjectId.isValid(id))) {
    return RepositoryModel.findById(id);
  } else {
    throw new Error('Invalid ID');
  }
}

export const findMany = async (searchParams: object = {}, offset = 0, limit = 20): Promise<Repository[]> => {
  return RepositoryModel.find(searchParams)
    .skip(offset)
    .limit(limit);
}

export const count = async (searchParams: object = {}): Promise<number> => {
  return RepositoryModel.count(searchParams);
}

export const update = async (id: string, changes: UpdateParams) : Promise<Repository | Repository[]> => {
  if(Array.isArray(id)) {
    await RepositoryModel.updateMany({
      _id: { $in: id }
    }, {
      $set: changes
    }).exec();
    return RepositoryModel.find({
      _id: { $in: id }
    });
  }
  if (ObjectId.isValid(id)) {
    return RepositoryModel.findByIdAndUpdate(id, {
      $set: changes
    });
  } else {
    throw new Error('Invalid ID');
  }
}

export const remove = async (id: string | string[]) => {
  const removeRepositoryAssets = async (id: string | string[]) => {
    let assets;
    if(Array.isArray(id)) {
      const repositoryIds = id.map(strId => new ObjectId(strId));
      assets = await findManyAssets({
        repository: { $in: repositoryIds }
      });
    } else {
      const repositoryId = new ObjectId(id);
      assets = await findManyAssets({
        repository: repositoryId
      });
    }
    const assetIds = assets.map(a => a._id.toString());
    return removeAssets(assetIds);
  }
  await removeRepositoryAssets(id);
  if(Array.isArray(id)) {
   return RepositoryModel.remove({
      _id: { $in: id}
    });
  } else {
    return RepositoryModel.remove({
      _id: id
    });
  }
}
