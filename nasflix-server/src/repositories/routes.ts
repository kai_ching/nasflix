import { getLogger } from '../logger';
import { Router, Request, Response } from 'express';
import { DocumentIdentifier, HttpError, Paginated, SearchParams } from '../HttpTypes';
import { Repository } from './Repository';
import { count, create, findById, findMany, update } from './repositoryController';

const logger = getLogger('Repository');

export const repositoryRoutes = (router: Router) => {
  logger.info(`Registering endpoints`);

  router.get('/repositories', async (req: Request<SearchParams>, res: Response<Paginated<Repository>>) => {
    const q = req.params.q;
    const offset = req.params.offset ?? 0;
    const limit = req.params.limit ?? 20;

    let searchParam = {};
    if (q) {
      // TODO: interpret query string
    }
    const docCount = await count(searchParam);
    const result = await findMany(searchParam, offset, limit);
    res.status(200)
      .json({
        docs: result,
        limit,
        offset,
        docCount
      });
  });

  router.get('/repositories/:id', async (req: Request<DocumentIdentifier>, res: Response<Repository | HttpError>) => {
    try {
      const id = req.params.id;
      const result = await findById(id);
      res.status(200).json(result);
    } catch (error) {
      res.status(400).json({
        message: error.message
      });
    }
  });

  router.post('/repositories', async (req: Request, res: Response<Repository>) => {
    const repository = req.body;
    const result = await create(repository);
    res.status(201)
      .json(result);
  });

  router.patch('/repository/:id', async (req, res: Response<Repository | Repository[] | HttpError>) => {
    const {id} = req.params;
    const changes = req.body;
    try {
      const result = await update(id, changes);
      res.status(200)
        .json(result);
    } catch (error) {
      res.status(400).json(error);
    }
  });
}
