import mongoose, { Schema } from 'mongoose';

export interface Repository {
  url: string;
  rootPath?: string;
  name: string;
  description: string;
  metadata: object;
}

const RepositorySchema = new Schema<Repository>({
  url: { type: String, required: true, trim: true },
  rootPath: { type: String, trim: true },
  name: { type: String, required: true, unique: true },
  description : { type: String, trim: true},
  metadata : { type: Object, default: {}}
});

export const model = mongoose.model('Repository', RepositorySchema);
