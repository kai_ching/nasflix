import { createServer } from 'http';
import mongoose from 'mongoose';
import { load as loadConfig } from 'node-config-yaml';
import { getLogger} from './logger';
import express, { json, Router } from 'express';

import { assetRoutes as registerAssetEndpoints } from './assets/routes';
import { repositoryRoutes as registerRepositoryEndpoints } from './repositories/routes';

const config = loadConfig('./config/config.yml');
const logger = getLogger();


const initExpress = (config) => {
  const router = Router();
  router.use(json());

  // register routes
  registerAssetEndpoints(router);
  registerRepositoryEndpoints(router);

  const app = express();
  app.use('/api', router);

  const port = config.app.port;
  const httpServer = createServer(app);
  httpServer.listen(port, () => {
    logger.info(`API endpoint listening on port ${config.app.port}`);
  });
}

mongoose.connect(config.database.url)
  .then(() => {
    logger.info(`Connected to ${config.database.url}`);
    initExpress(config);
  });
