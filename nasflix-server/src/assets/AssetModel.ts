import mongoose, { Schema, Types } from 'mongoose';
import { Asset } from './types';

export type AssetDocument = {
  repository: Types.ObjectId;
  path: string;
  contentType: string;
  hash: string;
  size: number;
  status: string;
  url?: string;
  metadata: object;
}

const AssetSchema = new Schema<AssetDocument>({
  repository:   { type: Schema.Types.ObjectId, required: true},
  path:         { type: String, required: true, trim: true },
  url:          { type: String, trim: true},
  contentType:  { type: String, default: 'application/octet-stream'},
  size:         { type: Number, required: true },
  status:       { type: String, enum: ['STAGED', 'PUBLISHED', 'REMOVED'], default: 'STAGED' },
  hash:         { type: String, required: true },
  metadata:     { type: Object, default: {}}
});

export const model = mongoose.model('Asset', AssetSchema);

export const toObject = (doc: AssetDocument) : Asset => ({
  ...doc,
  repository: doc.repository.toString()
});
