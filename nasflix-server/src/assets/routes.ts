import { getLogger } from '../logger';
import { count, create, findById, findMany, update } from './assetController';
import { Request, Response, Router } from 'express';
import { Asset } from './types';
import { toObject } from './AssetModel';
import { DocumentCollection, DocumentIdentifier, HttpError, Paginated, SearchParams } from '../HttpTypes';

const logger = getLogger('Assets');

export const assetRoutes = (router: Router) => {
  logger.info(`Registering endpoints`);
  router.get('/assets', async (req: Request<SearchParams>, res: Response<Paginated<Asset>>) => {
    const q = req.params.q;
    const offset = req.params.offset ?? 0;
    const limit = req.params.limit ?? 20;

    let searchParam = {};
    if (q) {
      // TODO: interpret query string
    }
    const docCount = await count(searchParam);
    const result = await findMany(searchParam, offset, limit);
    res.status(200)
      .json({
        docs: result,
        limit,
        offset,
        docCount
      });
  });

  router.get('/assets/:id', async (req: Request<DocumentIdentifier>, res: Response<Asset | HttpError >) => {
    try {
      const id = req.params.id;
      const result = await findById(id);
      res.status(200)
        .json(result);
    } catch (error) {
      res.status(400)
        .json({
          message: error.message
        });
    }
  });

  router.post('/assets/', async (req: Request, res: Response<Asset | HttpError >) => {
    const asset = req.body;
    const result = await create(asset);
    res.status(201)
      .json(toObject(result));
  });

  router.patch('/assets/:id', async (req, res: Response<Asset | Asset[] | HttpError>) => {
    const { id } = req.params;
    const changes = req.body;
    try {
      const result = await update(id, changes);
      res.status(200)
        .json(result);
    } catch(error) {
      res.status(400)
        .json(error)
    }
  });

  router.delete('/assets/', async (req, res) => {
    const ids = req.body;
    const result = await update(ids, {
      status: 'REMOVED'
    });

  })
}
