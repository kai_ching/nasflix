export type Asset = {
  repository: string;
  path: string;
  contentType: string;
  hash: string;
  size: number;
  status: string;
  url?: string;
  metadata: object;
}

export type Modification = {
  repository?: string;
  path?: string;
  contentType?: string;
  hash?: string;
  size?: number;
  status?: string;
  metadata?: string;
}
