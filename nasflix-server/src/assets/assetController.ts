import { Types } from 'mongoose';
import { AssetDocument, model as AssetModel, toObject } from "./AssetModel";
import { Asset, Modification } from './types';
import { findById as findRepositoryById} from '../repositories/repositoryController';

const ObjectId = Types.ObjectId;

export const create = async (asset: Asset) => {
  const repository = findRepositoryById(asset.repository);
  if(repository) {
    return AssetModel.create<AssetDocument>({
      ...asset,
      repository: new ObjectId(asset.repository)
    });
  } else {
    throw new Error(`No repository with Id ${asset.repository}`);
  }
}

export const findById = async (id: string): Promise<Asset> => {
  if (ObjectId.isValid(id)) {
    const result = await AssetModel.findById(id);
    return toObject(result);
  } else {
    throw new Error("Invalid ID");
  }
}

export const findMany = async (searchParams: object = {}, offset = 0, limit = 20): Promise<Asset[]> => {
  const result = await AssetModel.find(searchParams)
    .skip(offset)
    .limit(limit);
  return result.map(doc => toObject(doc));
}

export const count = async (searchParams: object = {}): Promise<number> => {
  return AssetModel.count(searchParams);
}

export const update = async (id: string | string[], changes: Modification): Promise<Asset | Asset[]> => {
  if (Array.isArray(id)) {
    await AssetModel.updateMany({
      _id: {$in: id}
    }, {
      $set: changes
    }).exec();
    const result = await AssetModel.find({
      _id: { $in: id}
    }).exec();
    result.map(doc => toObject(doc));
  } else {
    if (ObjectId.isValid(id)) {
      const result = await AssetModel.findByIdAndUpdate(id, {
        $set: changes
      });
      return toObject(result);
    } else {
      throw new Error('Invalid ID');
    }
  }
}

export const remove = async (id: string | string[]) => {
  if (Array.isArray(id)) {
    return AssetModel.remove({
      _id: {$in: id}
    });
  } else {
    return AssetModel.remove({
      _id: id
    });
  }
}
